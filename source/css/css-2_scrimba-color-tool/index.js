// test
const inputColorCode = document.querySelector(".inputColorBox");
const input = document.querySelector(".colorInput");

const hexCheck = (hex) => {
  if(!hex) return false;
  const onlyHex = hex.replace('#','');
  return onlyHex.length === 3 || onlyHex.length === 6;
}


input.addEventListener("keyup", () => {
  
  if (!hexCheck(input.value)) return;
    hexValue = input.value.replace('#','');
    inputColorCode.style.backgroundColor = '#' + hexValue;
});

const slideB = document.querySelector('.sliderButton');
const slideDiv = document.querySelector('.settingSlider');
slideDiv.addEventListener('click', function(){
  slideB.classList.toggle('slide-left');
  console.log('yes');
});

const convertHexToRGB = (hex) => {
  if(!isValidHex(hex)) return null;
  
  let strippedHex = hex.replace('#','');
  
  if(strippedHex.length === 3) {
    strippedHex = strippedHex[0] + strippedHex[0]
    + strippedHex[1] + strippedHex[1]
    + strippedHex[2] + strippedHex[2];
  }
  
  const r  = parseInt(strippedHex.substring(0,2), 16);
  const g  = parseInt(strippedHex.substring(2,4), 16);
  const b  = parseInt(strippedHex.substring(4,6), 16);
  
  return {r,g,b}
}

const convertRGBToHex = (r,g,b) => {
  const firstPair = ("0" + r.toString(16)).slice(-2);
  const secondPair = ("0" + g.toString(16)).slice(-2);
  const thirdPair = ("0" + b.toString(16)).slice(-2);
  
  const hex = "#" + firstPair + secondPair + thirdPair;
  return hex;
}

const sliderPer = document.querySelector('.slider');
const sliderText = document.getElementById('sliderText');

sliderPer.addEventListener('input', () => {
sliderText.textContent = `${sliderPer.value}%`;
})

const alterColor = (hex, percentage) => {
  const {r,g,b} = convertHexToRGB(hex);
  
  const amount = Math.floor((percentage/100) * 255);
  
  const newR = r + amount;
  const newG = g + amount;
  const newB = b + amount;
  console.log(newR, newG, newB)
  return convertRGBToHex(newR, newG, newB);
}
