#include <stdio.h>

int main(void) {

	// This is a singleline comment

	/*This is
	 *a multiline
	 *comment
	*/
	
	// \n is for paragraph
	printf("Hello World!\n");
	printf("I like pizza!\n");
	printf("I\nam\nan\napple\n");
	//\t is for tab space
	printf("1\t2\t3\t4\t5\n");
	// double quote is "\"text\"
	// single quote is "\'text\'
	printf("\"double quote test\"\n");
	printf("\'sinlge quote test\'\n");
	

	//from now on: variables
	//https://www.youtube.com/watch?v=87SH2Cn0s9A&list=WL&index=1&t=542s

	int x; //declaration
	x = 123; //initialization
		
	int age  = 23; //declaration + initizalization
	float gpa = 2.05; //floating point number
	char grade = 'C'; //single character
	char name[] = "Bro"; // array of characters, use double quote
	
	printf("Hello %s\n",name);
	printf("You are %d years old\n",age);
	printf("Your avarage grade is %c\n", grade);	
	printf("Your gpa is %f\n",gpa);
	//every variable is mentioned different
	


	return 0;
}

