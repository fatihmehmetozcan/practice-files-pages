// ==UserScript==
// @name     darkYT
// @namespace     https://www.youtube.com/
// @include     https://www.youtube.com/
// @version  0.1
// @grant    none
// ==/UserScript==

const timestage1 = 2000;
const timestage2 = 2000;

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, timestage1)
    } else {
        document.addEventListener("DOMContentLoaded", fn)
    };
};

let checkTheme;
//const checkTheme = document.querySelectorAll('html')[0].attributes['dark'].textContent.includes('true')
if(!document.querySelectorAll('html')[0].attributes['dark'] === undefined) (checktheme) => {return checkTheme};
console.log(checkTheme);

docReady(function () {
    let zaman = new Date()
    let saat = zaman.getHours()
    console.log(saat)
    if (saat >= 20 && !checkTheme) { trigger(2) }
    else if (saat <= 8 && !checkTheme) { trigger(2) }
    else if (saat > 8 && saat < 20 && checkTheme) { trigger(3) };
});

// function trigger(tema) {
//     let first = document.querySelector('#buttons.style-scope.ytd-masthead').childNodes[3];
//     setTimeout(first.click(), timestage2);

//     setTimeout(() => {
//         let second = document.querySelector('div#label.style-scope.ytd-toggle-theme-compact-link-renderer');
//         second.click();
//     }, timestage2);

//     setTimeout(() => {
//         let third = document.querySelectorAll("#submenu yt-multi-page-menu-section-renderer ytd-compact-link-renderer")[tema];
//         third.click();
//     }, timestage2);
// };


function trigger(theme) {

    setTimeout( () => {
        let b1 = document.querySelector('#buttons.style-scope.ytd-masthead').childNodes[3];
        b1.click();
        setTimeout( () => {
            let b2 = document.querySelector('div#label.style-scope.ytd-toggle-theme-compact-link-renderer');
            b2.click();

            setTimeout( () => {
                //let b3 = document.querySelectorAll('[compact-link-style=compact-link-style-type-selection-menu]')[theme];
                b3 = document.querySelectorAll("#submenu yt-multi-page-menu-section-renderer ytd-compact-link-renderer")[theme];

                b3.click();

            }, timestage2);

        }, timestage2);

    }, timestage2);

}