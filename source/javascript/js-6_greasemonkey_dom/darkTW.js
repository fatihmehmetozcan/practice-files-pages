// ==UserScript==
// @name     darkTW
// @namespace     https://www.twitch.tv/
// @include     https://www.twitch.tv/
// @version  0.1
// @grant    none
// ==/UserScript==

const timestage1 = 1500;

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, timestage1)
    } else {
        document.addEventListener("DOMContentLoaded", fn)
    };
};

docReady(function () {
    const tema = document.querySelector('.tw-root--hover.js-focus-visible').className.includes('light');
    console.log(tema);
    let zaman = new Date()
    let saat = zaman.getHours()
    console.log(saat)
    if (saat >= 20 && tema) { trigger() }
    else if (saat <= 8 && tema) { trigger() }
    else if (saat > 8 && saat < 20 && !tema) { trigger() };
});

function trigger() {
    //document.querySelectorAll(".user-menu-dropdown__main-menu")[0].querySelector(".simplebar-content").querySelectorAll("div")[100].click();
    //document.querySelector(".simplebar-content > .fzWsfN > div:not([class])").click();
    setTimeout(() => {
        setTimeout(() => {
            setTimeout(() => {
                document.querySelector("[data-a-target=user-menu-toggle]").click()
            }, timestage1);
            document.querySelectorAll(".tw-toggle__button")[2].click()
        }, timestage1); 
        document.querySelector("[data-a-target=user-menu-toggle]").click()
    }, timestage1);
};