// ==UserScript==
// @name     darkTS
// @namespace     https://teknoseyir.com/
// @include     https://teknoseyir.com/
// @version  1
// @grant    none
// ==/UserScript==

var timestage1 = 0;

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, timestage1)
    } else {
        document.addEventListener("DOMContentLoaded", fn)
    };
}

docReady(function () {
  
    let zaman = new Date();
    let saat = zaman.getHours();
    
    const getThemeText = document.getElementById('koyu_tema').textContent;
  	console.log(saat)
    if (saat >= 20 && getThemeText.includes('aç')) {
        document.querySelector("[data-action=koyu_tema]").click()
    }
  	else if(saat <= 8 && getThemeText.includes('aç')){
        document.querySelector("[data-action=koyu_tema]").click()
    }
    else if (saat > 8 && saat < 20 && getThemeText.includes('kapat')) {
        document.querySelector("[data-action=koyu_tema]").click()
    };
});