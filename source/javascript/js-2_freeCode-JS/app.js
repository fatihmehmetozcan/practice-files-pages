console.log("Hello World from JavaScript!");

/* ---Data types---

undefined, null, boolean, string, symbol, number, object
*/

/*
var nyName = "Fatih"

// sonradan yeni değer yazılabiliyor üstüne
myName = 8

let ourName = "freeCodeCamp"

//const sabit bir değişken, sonradan değiştirilemiyor
const pi = 3.14

*/


/* satır sonlarına ; koymak gerekli değil ama satırın nerede bitmesi kodun anlaşılması için gerekli olduğu için koymak faydalı

var a;
var b = 2;
console.log(a);
a = 7;

b = a;

console.log(a);

*/


/*
var quotient = 4.4 / 2.0;
console.log(quotient);

var remainder;
remainder = 11 % 3;
console.log(remainder);


// a = a + 5 gibi kendini kullanan işenlerdeki operatörü += gibi birleştirerek aynı sonuca ulaşmak mümkün
var a = 1;
var b = 1;
a = a + 5;
console.log(a);
b += 5;
console.log(b);

var c = 2;
var d = 2;
c = c * 6;
console.log(c);
d *= 6;
console.log(d);
*/



/*
var myStr = "I am a \"double qutoted\" string inside \"double quotes\"";
console.log(myStr);

var myStr = '<a href="http://www.example.com" target="_blank">Link</a>';
console.log(myStr);


var ourStr = "First part & " + "second part";
console.log(ourStr);

var ourStr = "I come first. ";
ourStr += "I come second."
console.log(ourStr);

console.log(ourStr.length);


var firstName = "Ada";
var firstLetterOfFirstName = "";

firstLetterOfFirstName = firstName[0];
console.log(firstLetterOfFirstName);

*/


/*
function wordBlanks(myNoun, myAdjective, myVerb, myAdverb) {
    var result = "";
    result += "The " + myAdjective + " " + myNoun + " " + myVerb + " to the store " + myAdjective

    return result;
}

console.log(wordBlanks("dog", "big", "ran", "quickly"));

*/



/*
var ourArray = [50, 60, 70];
console.log(ourArray[0]);
ourArray[0] = 99;
console.log(ourArray[0]);
*/


/*
var myArray = [[1, 2, 3], [4, 5, 6,], [7, 8, 9]];
var myData = myArray[1][0];
console.log(myData);

var ourArray = ["Stimpson", "J", "cat"];


ourArray.push(["happy", "joy"]);
// ourArray şöyle oldu: ["Stimpson", "J", "cat",["happy", "joy"]]
console.log(ourArray);

// .pop ile en sondakini çıkartmış oluyoruz
ourArray.pop();
console.log(ourArray);

// .shift ile en baştakini çıkartmış oluyoruz
// ourArray.shift();
var removedFromMyArray = ourArray.shift();
console.log(removedFromMyArray);

//.unshift başlangıca ekleme yapıyor.
ourArray.unshift("Happy");
console.log(ourArray)
*/





/*
function ourReusableFunction() {
    console.log("Heyya, World!");
}
ourReusableFunction();


function ourFunctionWithArgs(a, b) {
    console.log(a - b);
    console.log(a + b);
}
ourFunctionWithArgs(10, 5);

*/


/*
function minusSeven(num) {
    return num - 7;
}
console.log(minusSeven(8));

function timesFive(num) {
    return num * 5;
}
console.log(timesFive(5));

var fived = 0;
fived = timesFive(5);
console.log(fived);
*/



/*

function nextInLine(arr, item) {
    arr.push(item);
    return arr.shift();
}

var testArr = [1, 2, 3, 4, 5];

console.log("Before: " + JSON.stringify(testArr));
console.log(nextInLine(testArr, 6));
console.log("After: " + JSON.stringify(testArr));

*/


var newDiv = document.createElement("DIV");
newDiv.className = "subDiv";
newDiv.appendChild(document.createTextNode('IF STATEMENT (Video at 1:09:40)'));
document.getElementById("mainDiv").appendChild(newDiv);

/*
--- IF STATEMENT (Video at 1:09:40) ---
*/


function trueOrFalse(wasThatTrue) {
    if (wasThatTrue) {
        return "Yes, it is true";
    }
    return "No, it is not true";
}
console.log(trueOrFalse(true));

function testEqual(val) {
    if (val === 12) {
        return `${val} is equal to 12`;
    }
    return `${val} is not equal to 12`;
}
console.log(testEqual(12));

function compareEquality(a, b) {
    // == çift eşit ile karşılaştırma yapılırsa fonksiyon içinde sorulan integer ve string aynı tipe dönüştürülüyor
    // eğer === üç eşit ile karşılaştırma yapılırsa, herhangi bir çevir yapılmıyor, aşağıdaki duruma göre not equal sonucu verecektir
    if (a == b) {
        return `${a} is equal to ${b}`;
    }
    return `${a} is not equal to ${b}`;
}
console.log(compareEquality(10, '10'));

// IF ELSE

function testEqual(val2) {
    if (val2 === 12) {
        return `${val2} is equal to 12`;
    } else {
        return `${val2} is not equal to 12`;
    }
}
console.log(testEqual(12));


function testElseIf(val3) {
    if (val3 > 10) {
        return "Greater than 10";
    } else if (val3 < 5) {
        return "Smaller then 5";
    } else {
        return "Between 5 and 10";
    }
}
console.log(testElseIf(3));



function addElement(elementTextContent) {
    var newDiv = document.createElement("DIV");
    newDiv.className = "subDiv";
    newDiv.appendChild(document.createTextNode(elementTextContent));
    document.getElementById("mainDiv").appendChild(newDiv);
    // newDiv.style.marginLeft = '50px';
}

addElement('Objects at video 1:49:20');

var ourDog = {
    "name": "Camper",
    "legs": 4,
    "tails": 1,
    "friends": ["everything!"]
};
console.log(`Our dog's name is ` + ourDog.name);
ourDog.name = "Happy camper";
console.log(`Our dog's new name is ` + ourDog.name);

ourDog.bark = "woof woof";
console.log(`Our dog's new property is ` + ourDog.bark);



//   LOOPS
addElement('loops at video 2:10:15');

var i = 0;
while (i <= 5) {

    console.log(i);
    i++;
}

var testArray = [];
for (var k = 0; k < 5; k++) {
    testArray.push(k);
}
console.log(testArray);





// Arrow functions
addElement('arrow functions 2:47:17');


/* var magic = function () {
    return new Date();
}
*/
// eğer tek değer return ediliyorsa, tek satırda yazılabilir
const magic = () => new Date();



/* var myConcat = function (arr1, arr2) {
    return arr1.concat(arr2);
};
console.log(myConcat([111, 222], [333, 444, 555]));
*/
const myConcat = (arr1, arr2) => arr1.concat(arr2);
console.log(myConcat([111, 222], [333, 444, 555]));


//fonksiyonlarda istenilen değerin girilmemesi durumunda default değer kullanılabiliyor
function defaultValue(arr3, arr4 = 1) {
    return arr3 + " ve " + arr4;
}
console.log(defaultValue(5));
console.log(defaultValue(5, 6));


//rest operatör(üç nokta ...) fonksiyon istenilenlerini array yapıyor

const sum = (function () {
    // return function sum(x ,y ,z) {
    return function sum(...args) {
        //const args = [x, y, z];


        //reduce arrayde kullanılan bir metod
        return args.reduce((a, b) => a + b, 0);
    }
})();
console.log(sum(1, 2, 3, 4));


//variableları eşitlerken şu yöntem kullanılabilir
//Destructuring
addElement('Destructuring 2:57:18')

const AVG_TEMPERATURES = {
    today: 77,
    tomorrow: 80
};
function getTempOfTomorrow(avgTemperatures) {
    const { tomorrow: tempOfTomorrow } = avgTemperatures
    return tempOfTomorrow;
}
console.log(getTempOfTomorrow(AVG_TEMPERATURES));