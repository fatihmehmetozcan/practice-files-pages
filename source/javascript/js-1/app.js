/*
        --- Concatenation ---

const myName = "Fatih";

const myAge = 21;

console.log("Hello it's me " + myName + " and my age is " + myAge);

console.log(`Hello it's me ${myName} and my age is ${myAge}`)

*/


/*
const isim = "fatih";
const yas = 22;

const combined = isim + yas;

console.log(combined);
console.log(typeof combined);
console.log(typeof yas);
*/


/*

if else
const age = 16;

if (age > 18) {
    console.log("you can pass");
} else if (age < 15) {
    console.log("15ten küçük");
}
else {
    console.log("no way");
}
*/

/* const age = 18;

if (age === 18) { console.log("18 yaş"); }
else { console.log("18 değil"); } */


/*
        // --- arrays ----

const schedule = ['1', '2', '3'];

//console.log(schedule[2]);
//console.log(schedule.length);

schedule.push('ekleme');
//console.log(schedule[3]);
console.log(schedule);
schedule.pop(); // sonuncuyu atıyor arrayden
console.log(schedule);

schedule.shift(); //ilkini atıyor arrayden
console.log(schedule);

schedule.unshift("ilk başa ekleme");
console.log(schedule);

console.log(schedule.indexOf('ilk başa ekleme')); // içeriğin hangi sırada olduğunu getiriyor

const degisken = schedule.indexOf('ilk başa ekleme');
console.log(degisken);
console.log(schedule[degisken]);

*/


/*
        // --- objects ---

const user = {

    name: 'edwin',
    age: 24,
    married: false,
    purchases: ['phone', 'car ', 'laptop'],
    sayName: function () { console.log(this.name); }
};
//console.log(user.purchases);


function apples() { console.log('apple') };
apples();

//THIS - pencerenin özelliklerini getiriyor
console.log(this);
function testThis() { console.log(`test this: ${this}`) }
testThis();
//eğer object içinde olursa onu getiriyor :

user.sayName();
*/


/*
// --- LOOPS ---

const names = ['ed', 'john', 'eliza', 'burrito', 'harry', 'potter'];

for (each of names) {
    console.log(each)
    if (each === 'john') { console.log('found john, exiting'); break; }
}

let loading = 0;

while (loading < 100) { loading++; console.log(`still loading = ${loading}`); }
*/


/*
// new FileReader object
let reader = new FileReader();
const file = new File('/app.js');

// event fired when file reading finished
reader.addEventListener('load', function (e) {
    // contents of the file
    let text = e.target.result;
    document.querySelector("#file-contents").textContent = text;
});

// read file as text file
reader.readAsText(file);
*/