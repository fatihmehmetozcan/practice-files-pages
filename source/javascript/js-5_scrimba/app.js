/*
//js onyl have one type of number
let example = 7.77;

console.log(typeof example);
example = '7.77';
console.log(typeof example);
example = 5.55;
console.log(typeof example);

console.log(parseInt(example));
console.log(parseFloat(example));
console.log(typeof parseFloat(example));

console.log(example.toFixed(5));
//output: 5.55000
console.log(typeof example.toFixed(5));
//toFixed çevirince string olarak veriyor

let firstName = 'fatih';
let lastName = 'ozcan';
const fullName = `${firstName} ${lastName}`
console.log(fullName);
console.log(`${firstName} ${lastName}`.length);
console.log(`${firstName} ${lastName}`.toUpperCase());

*/





//Booelan
/*
let example1 = false;
let example2 = true;
let example3 = null;
let example4 = undefined;
let example5 = '';
let example6 = NaN;
let example7 = -5;
let example8 = 0;

console.log(Boolean(example1)); //false
console.log(Boolean(example2)); //true
console.log(Boolean(example3)); //false
console.log(Boolean(example4)); //false
console.log(Boolean(example5)); //false
console.log(Boolean(example6)); //false
console.log(Boolean(example7)); //false
console.log(Boolean(example8)); //true
*/






/*

//Arrays
console.clear();

let sampleArray = [1, 2, 3, 4];
sampleArray.push(5, 6, 7);
console.log(sampleArray);

//pop sondakini çıkarıyor
sampleArray.pop();
console.log(sampleArray);

sampleArray[0] = 1;

sampleArray.forEach((element) => {
    console.log(element)
});


//Array challenge
const list = ["toilet paper", "bottled water", "sanitizer"];

list[0] = "paper towels";
console.log(list);
list.pop();
console.log(list);
list.push('bleach');
console.log(list);
*/






/*
//Objects

let example1 = {
    firstName: 'Dylan',
    lastName: 'Israel',
    address: {
        city: 'Austin',
        state: 'Texas'
    },
    age: 30,
    cats: ['Milo', 'Tito', 'Achieles']
};

example1.age = 31;
console.log(Object.values(example1));
console.log(example1.hasOwnProperty('firstName'));


*/



//deneme
const getButton = document.querySelector('.btn');
const getText = document.querySelector('.input');

getButton.addEventListener('click', function () {
    if (getText.value === 'deneme') {
        console.log('doğru')
    } else {
        console.log('doğru değil')
    }

})




/*

//operator

let example1 = 10;
let example2 = '10';
console.log(typeof example1);
console.log(typeof example2);
//2 eşit sadece değeri karşılaştırırken, 3 eşit hem değeri hemde data tipini karşılaştırıyor
console.log(example1 == example2); //true
console.log(example1 === example2); //false


*/



//switch statements
// let studentAnswer = 'C';
let studentAnswer;

switch (studentAnswer) {

    case 'A':
        console.log('A is wrong');
        break;
    case 'B':
        console.log('B is wrong');
        break;
    case 'C':
        console.log('C is correct');
        break;
    default:
        console.log('Default answer');
}





// LOOPS

let total = 0;

for (let i = 0; i < 5; i++) {
    total += i;
}
console.log(total);


let numArray = [10, 20, 30, 40, 50, 60, 70, 80];
let total2 = 0;

for (let i = 0; i < numArray.length; i++) {

    total2 += numArray[i];

}
console.log(total2);



//challenge
const cartItems = [
    { quantity: 1, price: 5 },
    { quantity: 3, price: 4 },
    { quantity: 10, price: 1 },
];

let totalPrice = 0;
//for (let k = 0; k < cartItems.length; k++) {
//    totalPrice += cartItems[k].quantity * cartItems[k].price
//};

//farklı bir yöntem olarak
for (const cartItem of cartItems) {
    totalPrice += cartItem.quantity * cartItem.price
}
console.log(totalPrice * 1.08);



let count = 0;

while (count < 20) {
    count++;
}


while (true) {
    count++;
    if (count >= 20) {
        break;
    }
}

// do while olduğu zaman en azından bir kere uyguluyor durum false olsa dahi
do {
    count++;
    console.log(count);
    if (count >= 20) {
        break;
    }
    console.log(count);
}
while (false)




//FUNCTIONS

function add() {
    console.log('Add log')
};
add();


function logValue(entry) {

    console.log(entry)
};
logValue('test');


function logValue(entry1, entry2) {

    console.log(entry1 + " / " + entry2)
};
logValue('test1', 'test2');



//challenge


function getUserCredentials(firstName, lastName, email) {
    return firstName + ' ' + lastName + ' ' + email + ' users';
}

const shopItems = [
    { quantity: 1, price: 5 },
    { quantity: 3, price: 4 },
    { quantity: 10, price: 1 }
];

let preTaxTotal = 0;
function getPreTaxTotal() {
    for (cItem of shopItems) {
        preTaxTotal += cItem.quantity * cItem.price
    }
    return preTaxTotal;
}

function getTaxedTotal() {
    return preTaxTotal * 1.08;
};

console.log('User with credentials: ' + getUserCredentials('Fatih', 'Ozcan', 'mail@mail.com') + ' check out value without tax is: ' + getPreTaxTotal() + ' and with tax is ' + getTaxedTotal());
