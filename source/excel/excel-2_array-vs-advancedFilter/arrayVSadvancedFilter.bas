Attribute VB_Name = "arrayVSadvancedFilter"
'alttaki uygulama hucre yazdirma hizi karsilastirmasi konusunda reddit sitesinde ornek olarak verilmis
'bu videoda ise ayni sekilde array kullanimi mevcut. fakat son olarak yaptigi advanced filter ile daha hizli bir sonuc almis

'https://www.youtube.com/watch?v=GCSF5tq7pZ0
Private Sub arrayVSadvancedFilter()

TurnOffStuff

Dim rgf As Range
Set rgf = Sheet1.Range("A1").CurrentRegion

Dim criteriaRange As Range
Set criteriaRange = ThisWorkbook.Worksheets("AdvFilter").Range("A1").CurrentRegion

rgf.AdvancedFilter xlFilterCopy, criteriaRange, Sheet1.Range("A1:H1")

TurnOnStuff

End Sub

Private Function TurnOffStuff()

Application.ScreenUpdating = False
Application.Calculation = xlCalculationManual
Application.EnableEvents = False

End Function
Private Function TurnOnStuff()

Application.ScreenUpdating = True
Application.Calculation = xlManualAutomatic
Application.EnableEvents = True

End Function

'https://www.reddit.com/r/vba/comments/nuio9h/vba_best_practices_cheat_sheet/
'10k sayida hucre yazarken ekran guncelleme acik ve kapali haldeyken activecell.Value ile activecell.Offset(*,*).Value karsilastirmasi ve
'daha sonrasinda 10k sayida dongu yerine 10k degerin yazildigi Array'in Range("*").Resize metodu ile ekrana yazilmasi
'Sonuc olarak: Range("A2").Resize(UBound(Arry), 1) = Arry satiri ekran guncelleme acikken bile fark atiyor uygulama suresinde
'Screen Updating OFF : Cell activation duration: 1.113s
'Screen Updating ON : Cell activation duration: 14.719s
'Screen Updating OFF : Cell Offset duration: 0.48s
'Screen Updating ON : Cell Offset duration: 0.523s
'Screen Updating ON: Array+Resize duration: 0.012s
Private Sub Cell_Operations_are_slow()

Sheets.Add After:=Sheets(Sheets.Count)

Debug.Print Now()

Application.ScreenUpdating = False
For j = 1 To 2
    Columns("A").ClearContents
    If j = 2 Then
        Application.ScreenUpdating = True
    End If
    StrtTime = Timer
    Range("A2").Activate
    For i = 1 To 10000
        ActiveCell.Value = i
        ActiveCell.Offset(1, 0).Activate
    Next
    Select Case j
        Case 1: Debug.Print "Screen Updating OFF : Cell activation duration: " & Round(Timer - StrtTime, 3) & "s"
        Case 2: Debug.Print "Screen Updating ON : Cell activation duration: " & Round(Timer - StrtTime, 3) & "s"
    End Select
Next

Application.ScreenUpdating = False
For j = 1 To 2
    Columns("A").ClearContents
    If j = 2 Then
        Application.ScreenUpdating = True
    End If
    StrtTime = Timer
    Range("A2").Activate
    For i = 1 To 10000
        ActiveCell.Offset(i, 0).Value = i
    Next
    Select Case j
        Case 1: Debug.Print "Screen Updating OFF : Cell Offset duration: " & Round(Timer - StrtTime, 3) & "s"
        Case 2: Debug.Print "Screen Updating ON : Cell Offset duration: " & Round(Timer - StrtTime, 3) & "s"
    End Select
Next

Columns("A").ClearContents
MsgBox "Nothing up my sleeves...."
StrtTime = Timer
Dim Arry(1 To 10000, 1 To 1)  ' When applying Ranges, even 1 column is a 2D array.
For i = LBound(Arry) To UBound(Arry)
    Arry(i, 1) = i
Next
Range("A2").Resize(UBound(Arry), 1) = Arry
Debug.Print "Screen Updating ON: Array+Resize duration: " & Round(Timer - StrtTime, 3) & "s"

End Sub