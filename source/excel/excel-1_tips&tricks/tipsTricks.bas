Attribute VB_Name = "tipsTricks"
'5 Things I wish I knew When I started using Excel VBA
'https://www.youtube.com/watch?v=DxIzTKgchJ8


Private Sub worksheetCodeName()

'sol tarafta sheet kisminda yazan bilgilerde code name ve isim olarak iki farkli deger mevcut
'code name alarak isim farketmeksizin ozelliklerini cagirmak mumkun
MsgBox Sheet1.Name

End Sub



Private Sub currentRegion()

'excel icinde bir bolgede mevcut hucrelerin bolgesini sonuna kadar almak ctrl + * ile mumkun
Dim rg As Range
Set rg = Sheet1.Range("A1").currentRegion
MsgBox rg.Address

End Sub


Private Sub copyRangeToArray()

'hucreleri array icine kaydetmek
Dim arr As Variant
arr = Sheet1.Range("A1").currentRegion.Value

'arrayi hucrelere kaydetmek
Sheet1.Range("H1:K15").Value = arr

End Sub

Private Sub splitFunction()

'string icinde belli degere gore ayrim yapma
'https://youtu.be/DxIzTKgchJ8?t=512

End Sub

Private Sub completeWord()

'https://youtu.be/v-ETVJ83ivY?t=140
'ctrl + space tuslari ile kismen yazilmis variablelari tamamlamak ve cagirmak mumkun

End Sub


Private Sub watchWindow()

'degerleri takip etmek icin watch windows icinde watch ekleme vb.
'https://youtu.be/v-ETVJ83ivY?t=382

End Sub

Private Sub getRangeFromUser()

Dim rng As Range
'Set rng = Application.InputBox("Please enter range", Type:=1)
Set rng = Application.InputBox("Please enter range", Type:=8)
MsgBox rng.Address

'https://excelmacromastery.com/vba-inputbox/

End Sub


Private Sub readFromClosedWorkbook()

'kapali excelden veri okuma
'Reference eklenmesi gerekiyor tools-> references kismindan Microfost ActiveX Data Objects 6.1 Library
'https://excelmacromastery.com/excel-vba-copy/#Using_ADO_and_SQL

'dosya ismini getir
Dim fileName As String
fileName = "C:\Users\fam\Desktop\practice" & Application.PathSeparator & "consolidate.xlsm"
Debug.Print fileName

'baglantiyi kur
Dim conn As New ADODB.Connection
conn.Open "Provider=Microsoft.ACE.OLEDB.12.0;" & "Data Source=" & fileName & ";" & _
"Extended Properties=""Excel 12.0;HDR=Yes;"";"

'sql query olustur
Dim query As String
'query = 'Select * from [Sales$] where [First name]='Alan' "
query = "Select [First Name], Sum(Amount) From [Sales$] Group by [First Name]"

'workbook icinden veri al
Dim rs As New Recordset
rs.Open query, conn

'veri yaz
Sheet1.Cells.ClearContents
Sheet1.Range("A1").CopyFromRecordset rs

'baglantiyi kapa
conn.Close


End Sub
